//
//  HWDateUtilsTests.swift
//  HWDateUtilsTests
//
//  Created by HyunWoo-Kim on 2018. 6. 9..
//  Copyright © 2018년 HyunWoo-Kim. All rights reserved.
//

import XCTest
@testable import HWDateUtils

class HWDateUtilsTests: XCTestCase {
    
    private var dfmt: DateFormatter!
    let dateFormat = HWDateUtils.DEFAULT_DATE_FORMAT
    let timeFormat = HWDateUtils.DEFAULT_TIME_FORMAT
    override func setUp() {
        super.setUp()
        dfmt = DateFormatter()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGetDateString() {
        let date = Date()
        let result = HWDateUtils.getDateString(date: date, dateFormat:dateFormat, tz: .current)
    
        dfmt.dateFormat = dateFormat
    
        XCTAssertEqual(result, dfmt.string(from: date))
        
    }
    
    func testGetDateTimeString() {
        let date = Date()
        let result = HWDateUtils.getDateTimeString(date: date)
        dfmt.dateFormat = timeFormat
        
        XCTAssertEqual(result, dfmt.string(from: date))
        
    }
    
    func testAddDay() {
        let date = HWDateUtils.addDay(day: 1)
        let comp = Calendar.current.dateComponents(in: .current, from: Date())
        let resultComp = Calendar.current.dateComponents(in: .current, from: date!)
        
        XCTAssertEqual(comp.day! + 1, resultComp.day)
    }
    
    func testAddMonth() {
        let date = HWDateUtils.addMonths(month: 1)
        let comp = Calendar.current.dateComponents(in: .current, from: Date())
        let resultComp = Calendar.current.dateComponents(in: .current, from: date!)
        
        XCTAssertEqual(comp.month! + 1, resultComp.month)
    }
    
    
    func testAddYears() {
        let date = HWDateUtils.addYears(year: 1)
        let comp = Calendar.current.dateComponents(in: .current, from: Date())
        let resultComp = Calendar.current.dateComponents(in: .current, from: date!)

        XCTAssertEqual(comp.year! + 1, resultComp.year)
        
    }
 
    
    
    func testGetWeekDay() {
        let date = Date()
        let result = HWDateUtils.getWeekDay(date: date)
        
        let comp = Calendar.current.dateComponents(in: .current, from: date)
        
        XCTAssertEqual(result, comp.weekday)
    }
    
    func testGetDay() {
        let date = Date()
        let result = HWDateUtils.getDay(date: date)
        
        let comp = Calendar.current.dateComponents(in: .current, from: date)
        
        XCTAssertEqual(result, comp.day)
    }
    
    func testGetMonth() {
        let date = Date()
        let result = HWDateUtils.getMonth(date: date)
        
        let comp = Calendar.current.dateComponents(in: .current, from: date)
        
        XCTAssertEqual(result, comp.month)
    }
    
    
    func testGetYear() {
        let date = Date()
        let result = HWDateUtils.getYear(date: date)
        let comp = Calendar.current.dateComponents(in: .current, from: date)
        
        XCTAssertEqual(result, comp.year)
        
    }

    
}
