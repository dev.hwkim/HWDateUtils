//
//  HWDateUtils.swift
//  HWDateUtils
//
//  Created by HyunWoo-Kim on 2018. 6. 9..
//  Copyright © 2018년 HyunWoo-Kim. All rights reserved.
//

import Foundation


public class HWDateUtils {
    
    
    public static let DEFAULT_DATE_FORMAT = "yyyy/MM/dd"
    public static let DEFAULT_TIME_FORMAT = "HH:mm:ss";
    
    
    public static func convertStringToDate(strDate: String, dateFormat: String) -> Date? {
        let dfmt = DateFormatter()
        dfmt.dateFormat = dateFormat
        let date  = dfmt.date(from: strDate)
        return date
    }
    
    // MARK: - Date
    public static func getDateString(dateFormat: String, tz:TimeZone? = nil) -> String {
        return getDateString(date: Date(), dateFormat: dateFormat, tz: tz)
    }
    
    public static func getDateString(date:Date, dateFormat: String, tz: TimeZone? = nil) -> String {
        let dfmt = DateFormatter()
        dfmt.dateFormat = dateFormat
        if let tz = tz {
            dfmt.timeZone = tz
        }
        return dfmt.string(from: date)
    }
    
    public static func getDateString(date:Date, dateFormat: String, tzAbbreviation: String) -> String {
      let tz = TimeZone(abbreviation: tzAbbreviation)
        return getDateString(date: date, dateFormat: dateFormat, tz: tz)
    }
    
    public static func getDateString(date:Date, dateFormat: String) -> String {
        let dfmt = DateFormatter()
        dfmt.dateFormat = dateFormat
        return dfmt.string(from: date)
    }
    
    // MARK: - Time
    public static func getDateTimeString(date:Date, tzAbbreviation: String) -> String {
        let tz = TimeZone(abbreviation: tzAbbreviation)
        return getDateTimeString(date: date, tz: tz)
    }
    
    public static func getDateTimeString(timeIntervalSince1970:TimeInterval, tzAbbreviation: String) -> String {
        let date = Date(timeIntervalSince1970: timeIntervalSince1970)
        return getDateTimeString(date: date, tzAbbreviation: tzAbbreviation)
    }
    
    public static func getDateTimeString(date: Date, tz:TimeZone? = .current) -> String {
        return getDateString(date: Date(), dateFormat:DEFAULT_TIME_FORMAT , tz: tz)
    }
    
    // MARK: - Add Date
    public static func addDay(day: Int) -> Date? {
        return self.addDays(date: Date(), day: day)
    }
    
    public static func addDays(date:Date, day:Int) -> Date? {
        return Calendar.current.date(byAdding: .day, value: day, to: date)
    }
    


    public static func addMonths(date:Date, month:Int) -> Date? {
        return Calendar.current.date(byAdding: .month, value: month, to: date)
    }
    
    public static func addMonths(month: Int) -> Date? {
        return Calendar.current.date(byAdding: .month, value: month, to: Date())
    }
    
    
    public static func addYears(date:Date, year:Int) -> Date? {
        return Calendar.current.date(byAdding: .year, value: year, to: date)
    }
    
    public static func addYears(year: Int) -> Date? {
        return Calendar.current.date(byAdding: .year, value: year, to: Date())
    }
    
    // MARK: - Add Time

    public static func addSeconds(seconds: Int) -> Date? {
        return self.addSeconds(date: Date(), seconds: seconds)
    }
    
    public static func addSeconds(date:Date, seconds:Int) -> Date? {
        return Calendar.current.date(byAdding: .second, value: seconds, to: date)
    }
    
    
    public static func addMinute(minute: Int) -> Date? {
        return self.addMinute(date: Date(), minute: minute)
    }
    
    public static func addMinute(date:Date, minute:Int) -> Date? {
        return Calendar.current.date(byAdding: .minute, value: minute, to: date)
    }
    
    
    public static func addHour(hour: Int) -> Date? {
        return self.addHour(date: Date(), hour: hour)
    }
    
    public static func addHour(date:Date, hour:Int) -> Date? {
        return Calendar.current.date(byAdding: .hour, value: hour, to: date)
    }
    
    
    
    
    // MARK: - Get Date
    public static func getWeekDay(date: Date, tz: TimeZone = .current) -> Int? {
        let comp = Calendar.current.dateComponents(in: tz, from: date)
        return comp.weekday
    }
    
    public static func getDay(date: Date, tz: TimeZone = .current) -> Int? {
        let comp = Calendar.current.dateComponents(in: tz, from: date)
        return comp.day
    }
    
    
    public static func getMonth(date: Date, tz: TimeZone = .current) -> Int? {
        let comp = Calendar.current.dateComponents(in: tz, from: date)
        return comp.month
    }
    
    public static func getYear(date: Date, tz: TimeZone = .current) -> Int? {
        let comp = Calendar.current.dateComponents(in: tz, from: date)
        return comp.year
    }
    
    // MARK: - Get Time
    public static func getMinute(date: Date, tz: TimeZone = .current) -> Int? {
        let comp = Calendar.current.dateComponents(in: tz, from: date)
        return comp.minute
    }
    
    
    
    public static func getHour(date: Date, tz: TimeZone = .current) -> Int? {
        let comp = Calendar.current.dateComponents(in: tz, from: date)
        return comp.hour
    }
    

    
}
